import { nanoid } from "nanoid";
import { dataCategory } from "../../data";

import styles from "./cardsHeader.module.css";

function CardsHeader({ selectProduct, product }) {
  const { isSelected } = product;
  return (
    <>
      {dataCategory.map((item) => {
        return (
          <div
            onClick={(e) => {
              if (!isSelected) {
                selectProduct(item.category);
              } else {
                e.preventDefault();
              }
            }}
            key={nanoid()}
            className={styles.card}
          >
            <img className={styles.cardImg} src={item.image} alt={item.title} />
            <h3>{item.category}</h3>
          </div>
        );
      })}
    </>
  );
}

export default CardsHeader;
