import { RiHeart3Fill } from "react-icons/ri";
import Stars from "../stars";
import Button from "../button";

import styles from "./card.module.css";

function Card({
  image,
  name,
  title,
  price,
  description,
  id,
  rating,
  addToCart,
  clickHeart,
}) {
  return (
    <div className={styles.productCards}>
      <img className={styles.cardImg} src={image} alt={name} />
      <h3>{title}</h3>
      <p className={styles.cardDescription}>{description}</p>
      <small className={styles.cards}>in stock</small>
      <Stars rating={rating} />
      <div className={styles.cardPrice}>{price.toFixed(2)} USD</div>
      <div className={styles.cardBottom}>
        <Button onClick={addToCart} id={id} />

        <RiHeart3Fill
          className={styles.cardIcon}
          onClick={(e) => {
            clickHeart(id);
          }}
        />
      </div>
    </div>
  );
}

export default Card;
