import CardsHeader from "../cardsHeader";
import styles from "./header.module.css";

function Header({ selectProduct, showProduct, product }) {
  const { isSelected } = product;
  return (
    <div className={styles.header}>
      <h4
        onClick={(e) => {
          showProduct();
        }}
      >
        Catalog
      </h4>
      <h3> {`${isSelected ? isSelected : ""}`}</h3>
      <div className={styles.headerBox}>
        <CardsHeader selectProduct={selectProduct} product={product} />
      </div>
    </div>
  );
}

export default Header;
