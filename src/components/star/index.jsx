import styles from "./star.module.css";

function Star({ rating = false }) {
  return <div className={rating ? styles.circle : styles.circleActive}></div>;
}

export default Star;
