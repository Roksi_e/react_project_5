import { nanoid } from "nanoid";
import Card from "../card";

function Cards({ product, addToCart, clickHeart }) {
  const { error, isLoaded, products } = product;

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return (
      <>
        <h1>Loading...</h1>;
      </>
    );
  } else {
    return (
      <>
        {products.map((card) => {
          return (
            <Card
              key={nanoid()}
              {...card}
              addToCart={addToCart}
              clickHeart={clickHeart}
            />
          );
        })}
      </>
    );
  }
}

export default Cards;
