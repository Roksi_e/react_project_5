import styles from "./button.module.css";

function Button({ onClick, id }) {
  return (
    <>
      <button
        className={styles.button}
        onClick={(e) => {
          onClick(id);
        }}
      >
        Add to cart
      </button>
    </>
  );
}

export default Button;
