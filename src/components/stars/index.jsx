import Star from "../star";

import styles from "./stars.module.css";

function Stars({ rating }) {
  const array = new Array(5),
    stars = array.fill(<Star></Star>);
  return (
    <>
      <div className={styles.rating}>
        <div className={styles.divCircle}>
          <small>{rating.rate}</small>
          {Array.isArray(array)
            ? stars.map((e, i) => {
                return <Star rating={i < rating.rate} key={i}></Star>;
              })
            : console.log("error")}
        </div>
        <small>{rating.count} voices</small>
      </div>
    </>
  );
}
export default Stars;
