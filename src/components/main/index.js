import { Component } from "react";
import Cards from "../cards";
import Header from "../header";

import styles from "./main.module.css";

const url = "https://fakestoreapi.com/products";

class Main extends Component {
  state = {
    error: null,
    isLoaded: false,
    products: [],
    isSelected: "",
    isChecked: false,
  };

  showCard = () => {
    fetch(url)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            isSelected: "",
            products: [...result],
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  };

  addToCart = (id) => {
    console.log(
      this.state.products.find((item) => {
        return item.id === id;
      })
    );
  };

  clickHeart = (id) => {
    console.log(
      this.state.products.find((item) => {
        return item.id === id;
      })
    );
  };

  selectCard = (value) => {
    this.setState((state) => {
      return {
        ...state,
        isSelected: value.toUpperCase(),
        products: state.products.filter((item) => {
          return item.category === value;
        }),
      };
    });
  };

  componentDidMount() {
    this.showCard();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.products !== this.state.products) {
      return;
    }
    this.setState((state) => {
      return {
        ...state,
        products: state.products,
      };
    });
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <Header
          selectProduct={this.selectCard}
          product={this.state}
          showProduct={this.showCard}
        />
        <div className={styles.productsBox}>
          <Cards
            product={this.state}
            addToCart={this.addToCart}
            clickHeart={this.clickHeart}
          />
        </div>
      </div>
    );
  }
}

export default Main;
