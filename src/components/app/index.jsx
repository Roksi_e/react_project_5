import Main from "../main";
import "./app.css";

const App = () => {
  return (
    <>
      <Main />
    </>
  );
};

export default App;
